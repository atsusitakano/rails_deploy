json.extract! building, :id, :name, :address, :price, :station_name, :from_minutes, :created_at, :updated_at
json.url building_url(building, format: :json)
