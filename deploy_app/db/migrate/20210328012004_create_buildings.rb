class CreateBuildings < ActiveRecord::Migration[5.2]
  def change
    create_table :buildings do |t|
      t.string :name
      t.string :address
      t.integer :price
      t.string :station_name
      t.integer :from_minutes

      t.timestamps
    end
  end
end
